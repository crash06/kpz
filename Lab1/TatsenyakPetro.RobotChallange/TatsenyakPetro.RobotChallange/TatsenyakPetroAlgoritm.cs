﻿using Robot.Common;
using System;
using System.Collections.Generic;
using System.Text;


namespace TatsenyakPetro.RobotChallange
{
    public class TatsenyakPetroAlgoritm : IRobotAlgorithm
    {
        public string Author => "Tatsenyak Petro";

        public RobotCommand DoStep(IList<Robot.Common.Robot> robots, int robotToMoveIndex, Map map)
        {
            Robot.Common.Robot movingRobot = robots[robotToMoveIndex];
            if ((movingRobot.Energy > 500) && (robots.Count < map.Stations.Count))
            {
                return new CreateNewRobotCommand();
            }

            var newPosition = robots[robotToMoveIndex].Position;

            newPosition.X += 1;
            newPosition.Y += 1;

            return new MoveCommand() { NewPosition = newPosition };


        }
    }
}
